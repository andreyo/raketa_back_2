'use strict';

class Storage {
	constructor() {
		this.map = {};
	}

	set(key, value) {
		this.map[key] = value;
	}

	get(key) {
		return this.map[key];
	}

	remove(key) {
		delete this.map[key];
		return true;
	}
}

module.exports = new Storage();