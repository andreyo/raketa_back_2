import mediator from 'src/mediator';
import async from 'async';
import uuid from 'uuid';
import _ from 'lodash';
var log = require('lib/logger')('MODULE_ROLES');

import request from 'request';

class Roles {
	constructor({config, connections}) {
		this.config = config;
		this.backend = connections.backend;

		mediator.on('ws:message:roles:find', this.onGetRoles.bind(this));
		mediator.on('ws:message:roles:add', this.onCreateRole.bind(this));
		mediator.on('ws:message:roles:update', this.onUpdateRoles.bind(this));
		mediator.on('ws:message:roles:delete', this.onDeleteRoles.bind(this));
		mediator.on('ws:message:roles:getPermissions', this.onGetPermissions.bind(this));
		mediator.on('ws:message:roles:addPermissions', this.onAddPermissions.bind(this));
		mediator.on('ws:message:roles:deletePermissions', this.onDeletePermissions.bind(this));

		this.backend.bindMethods("findRoles", "/api/v1.1/organizations/${id}/roles", "GET");
		this.backend.bindMethods("createRole", "/api/v1.1/organizations/${id}/roles", "POST");
		this.backend.bindMethods("updateRoles", "/api/v1.1/organizations/${id}/roles/${roleId}", "PUT");
		this.backend.bindMethods("deleteRoles", "/api/v1.1/organizations/${id}/roles/${roleId}", "DELETE");
		this.backend.bindMethods("getPermissions", "/api/v1.1/organizations/${id}/roles/${roleId}/permissions", "GET");
		this.backend.bindMethods("addPermissions", "/api/v1.1/organizations/${id}/roles/${roleId}/permissions", "POST");
		this.backend.bindMethods("removePermissions", "/api/v1.1/organizations/${id}/roles/${roleId}/permissions", "DELETE");

		log.info('Module `Roles` inited');
	}

	onGetRoles(data, options, callback) {
		var args = {
			path: {
				id: data.organization_id || options.session.organization.id
			},
			token: options.session.token.access_token
		};

		this.backend.call('findRoles', args, (err, result, statusCode) => {
			if (err) {
				return callback(err);
			}
			if (statusCode >= 200 && statusCode < 400) {
				return callback(null, result);
			}
			callback(err);
		})
	}

	onGetPermissions(data, options, callback) {
		var args = {
			path: {
				id: data.organization_id || options.session.organization.id,
				roleId: parseInt(data.id)
			},
			token: options.session.token.access_token
		};

		this.backend.call('getPermissions', args, (err, result, statusCode) => {
			if (err) {
				return callback(err);
			}
			if (statusCode >= 200 && statusCode < 400) {
				return callback(null, result);
			}
			callback(err);
		})
	}

	onAddPermissions(data, options, callback) {
        var args = {
            path: {
				id: data.organization_id || options.session.organization.id,
				roleId: parseInt(data.roleId)
            },
            data: {
                permissions: data.permissions
            },
            token: options.session.token.access_token
        };

        this.backend.call('addPermissions', args, (err, result, statusCode) => {
            if (err) {
                return callback(err);
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, result);
            }
            callback(err);
        })
    }

	onDeletePermissions(data, options, callback) {
        var args = {
            path: {
				id: data.organization_id || options.session.organization.id,
				roleId: parseInt(data.roleId)
            },
            data: {
                permissions: data.permissions
            },
            token: options.session.token.access_token
        };

        this.backend.call('removePermissions', args, (err, result, statusCode) => {
            if (err) {
                return callback(err);
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, result);
            }
            callback(err);
        })
    }

	onCreateRole(data, options, callback) {
		var args = {
			path: {
				id: data.organization_id || options.session.organization.id
			},
			data: {
				name: data.name
			},
			token: options.session.token.access_token
		};

		this.backend.call('createRole', args, (err, result, statusCode) => {
			if (err) {
				return callback(err);
			}
			if (statusCode == 201) {
				return callback(null, result);
			}
			callback(err);
		})
	}

	onUpdateRoles(data, options, callback) {
		var args = {
			path: {
				id: data.organization_id || options.session.organization.id,
				roleId: data.id
			},
			data: {
				name: data.name
			},
			token: options.session.token.access_token
		};

		this.backend.call('updateRoles', args, (err, result, statusCode) => {
			if (err) {
				return callback(err);
			}
			if (statusCode == 202) {
				return callback(null, true);
			}
			callback(err);
		})
	}

	onDeleteRoles(data, options, callback) {
		var args = {
			path: {
				id: data.organization_id || options.session.organization.id,
				roleId: data.id
			},
			token: options.session.token.access_token
		};

		this.backend.call('deleteRoles', args, (err, result, statusCode) => {
			if (err) {
				return callback(err);
			}
			if (statusCode == 204) {
				return callback(null, true);
			}
			callback(err);
		})
	}

	findRoles(accessToken, data, callback) {

	}

}

export default Roles;