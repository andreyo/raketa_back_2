import Mediator from 'src/mediator';
import request from 'request';
import _ from 'lodash';
var log = require('lib/logger')('MODULE_OFFERS');

export default class Offers {
    constructor({config, connections}) {
        this.config = config;
        this.connections = connections;
        this.backend = connections.backend;

        Mediator.on('ws:message:offers:search', this.onMessage.bind(this));

        this.backend.bindMethods("searchOffers", "/api/v1.1/offers", "GET");

        log.info('Module `Offers` inited');
    }

    onMessage(data, options, callback) {
        var accessToken = _.get(options, ['session', 'token', 'access_token']);

        var args = {
            parameters: data,
            token: accessToken
        };

        this.backend.call('searchOffers', args, (err, data, statusCode) => {
            if (err) {
                return callback('INTERNAL_ERROR');
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, data);
            }
            return callback('INTERNAL_ERROR');
        });
    }
}