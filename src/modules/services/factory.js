import {readdir as readDir} from 'fs';
import _ from 'lodash';
var log = require('lib/logger')('SERVICES_FACTORY');

export default class ServiceFactory {
	constructor({config, connections}) {
		this.services = {};
		this.config = config;
		this.connections = connections;
		this.feelServices();
	}

	feelServices() {
		readDir(__dirname, (err, list) => {
			list.forEach(item => {
				if (['index.js', 'factory.js'].indexOf(item) == -1) {
					try {
						var service = new (require(__dirname + '/' + item))({
							config: this.config,
							connections: this.connections
						});
						this.services[service.service] = service;
					}
					catch(e){
						log.error('Can not get service', item, e);
					}
				}
			});
		});
	}

	get(serviceName) {
		return this.services[serviceName];
	}
}