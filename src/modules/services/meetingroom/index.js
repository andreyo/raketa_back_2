import request from 'request';
import _ from 'lodash';
import mediator from 'src/mediator';
var log = require('lib/logger')('SERVICES_MEETINGROOM');

// TODO: Удалить все это и работать через стандартный интерфейс офферов и услуг.

export default class MeetingRoom {
	constructor({config, connections}) {
		this.service = 'meeting_room';
		this.config = config[this.service];
		this.connections = connections;
		this.backend = connections.backend;

		/*var path = 'http://' + this.config.host + ':' + this.config.port;
		this.path = {
			search: path + '/api/slots',
			book: path + '/api/slots',
			cancelBook: path + '/api/slots/<id>'
		}*/

		//this.backend.bindMethods("getUser", "/api/v1.1/users/${id}", "GET");
		this.backend.bindMethods("search", "/api/v1.1/offers", "GET");
	}

	search(data, options, callback) {
		var accessToken = _.get(options, ['session', 'token', 'access_token']);

		var args = {
			parameters: {
				type: this.service,
				fromDate: data.start,
				toDate: data.end
			},
			token: accessToken
		};

		this.backend.call('search', args, (err, data, statusCode) => {
			if (err) {
				return callback('INTERNAL_ERROR');
			}
			if (statusCode >= 200 && statusCode < 400) {
				return callback(null, data.slots.map(item => ({
					id: item.id,
					start: item.start,
					stop: item.stop,
					amount: item.amount,
					currency: item.currency
				})));
			}
			return callback('INTERNAL_ERROR');
		});

		/*this.backend.call('getUser', args, function(err, data, statusCode) {
			log.debug('getUser '+id+' response', err, data, statusCode);
			if (err) {
				return callback('INTERNAL_ERROR');
			}

			if (statusCode >= 200 && statusCode < 400) {
				return callback(null, data);
			}
			return callback('USER_NOT_FOUND');
		});*/
		/*request.get(this.path.search + '?start=' + data.start + '&end=' + data.end, function(err, response, body){
			if (err) {
				log.error('Request error: ', err);
				return callback('CANT_SEARCH_MEETINGROOM');
			}

			try {
				var parsed = JSON.parse(body);
				callback(null, parsed);
				mediator.emit('service:search', this.service, data);
			}
			catch(e) {
				log.error('Parse error: ', e, body);
				return callback('CANT_SEARCH_MEETINGROOM');
			}
		}.bind(this));*/
	}

	book(data, callback) {
		var event = {
			start: data.start,
			end: data.end,
			owner: data.owner
		};

		request.post(this.path.book, {form: event}, function(err, res, body){
			if (err) {
				log.error('Request error: ', err);
				return callback('CANT_SEARCH_MEETINGROOM');
			}

			var response = res.toJSON();
			if(response.statusCode !== 200) {
				log.error('Request error, statusCode ' + response.statusCode + ': ', body);
				return callback('CANT_SEARCH_MEETINGROOM');
			}

			try {
				var parsed = JSON.parse(body);
				callback(null, parsed);
				mediator.emit('service:book', this.service, parsed);
			}
			catch(e) {
				log.error('Parse error: ', e, body);
				return callback('CANT_SEARCH_MEETINGROOM');
			}
		}.bind(this));
	}

	cancelBook(data, callback) {
		if(!data.id) {
			console.log('Attempt to delete all documents', data);
			return;
		}

		request.del(this.path.cancelBook.replace('<id>', data.id), function(err, res, body){
			if (err) {
				log.error('Request error: ', err);
				return callback('CANT_SEARCH_MEETINGROOM');
			}

			var response = res.toJSON();
			if(response.statusCode !== 204) {
				log.error('Request error, statusCode ' + response.statusCode + ': ', body);
				return callback('CANT_SEARCH_MEETINGROOM');
			}

			callback(null, true);
			mediator.emit('service:cancelBook', this.service, {id: data.id});
		}.bind(this));
	}
}