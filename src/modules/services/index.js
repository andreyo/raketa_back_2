import Mediator from 'src/mediator';
import ServiceFactory from './factory.js';
import request from 'request';
var log = require('lib/logger')('MODULE_SERVICES');

export default class Services {
	constructor({config, connections}) {
		this.config = config;
		this.connections = connections;
		this.factory = new ServiceFactory({
			config: this.config,
			connections: this.connections
		});
		this.backend = connections.backend;

		Mediator.on('ws:message:service:search', this.onMessage.bind(this));
		Mediator.on('ws:message:service:types', this.onGetServiceTypes.bind(this));
		Mediator.on('ws:message:service:all', this.getAllServices.bind(this));

		//Mediator.on('ws:connect', this.onClientConnect.bind(this));
		//Mediator.on('ws:message', this.onMessage.bind(this));
		//Mediator.on('service:book', this.onBook.bind(this))
		//Mediator.on('service:cancelBook', this.onCancelBook.bind(this))
		this.backend.bindMethods("serviceTypes", "/api/v1.1/servicetypes", "GET");

		log.info('Module `Services` inited');
	}

	onClientConnect(socketId) {
		//
	}

	onMessage(data, options, callback) {
		var Service = this.factory.get(data.service);
		if (!Service) return;

		Service.search(data.data, options, callback);
	}

	onBook(serviceName, data) {
		Mediator.emit('ws:broadcast', 'service:change', {
			action: 'book',
			service: serviceName,
			data: data
		});
	}

	onCancelBook(serviceName, data) {
		Mediator.emit('ws:broadcast', 'service:change', {
			action: 'cancelBook',
			service: serviceName,
			data: data
		});
	}

	getAllServices() {

	}

	onGetServiceTypes(data, options, callback) {
		console.log(options.session.token.access_token);
		this.getServiceTypes(options.session.token.access_token, callback);
	}

	getServiceTypes(accessToken, callback) {
		this.backend.call('serviceTypes', {token: accessToken}, function(err, result, statusCode) {
			if (err) {
				return callback('INTERNAL_ERROR');
			}

			callback(null, result);
		});
	}
}