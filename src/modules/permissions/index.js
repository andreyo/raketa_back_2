import mediator from 'src/mediator';
import async from 'async';
import uuid from 'uuid';
import _ from 'lodash';
var log = require('lib/logger')('MODULE_PERMISSIONS');

import request from 'request';

class Permissions {
	constructor({config, connections}) {
		this.config = config;
		this.backend = connections.backend;

		mediator.on('ws:message:permissions:find', this.onGetPermissions.bind(this));

		this.backend.bindMethods("findPermissions", "/api/v1.1/permissions", "GET");

		log.info('Module `Permissions` inited');
	}

	onGetPermissions(data, options, callback) {
		var args = {
			path: {
				id: data.organization_id || options.session.organization.id
			},
			token: options.session.token.access_token
		};

		this.backend.call('findPermissions', args, (err, result, statusCode) => {
			if (err) {
				return callback(err);
			}
			if (statusCode >= 200 && statusCode < 400) {
				return callback(null, result);
			}
			callback(err);
		})
	}

}

export default Permissions;