import mediator from 'src/mediator';
import request from 'request';
import _ from 'lodash';
var log = require('lib/logger')('MODULE_ORDERS');

export default class Orders {
    constructor({config, connections}) {
        this.config = config;
        this.connections = connections;
        this.backend = connections.backend;

        mediator.on('ws:message:orders:find', this.findOrders.bind(this));
        mediator.on('ws:message:orders:create', this.onCreateOrder.bind(this));
        mediator.on('ws:message:orders:services:find', this.findServices.bind(this));
        mediator.on('ws:message:orders:services:findByOrderId', this.findOrderServices.bind(this));
        mediator.on('ws:message:orders:services:create', this.createService.bind(this));
        mediator.on('ws:message:orders:services:get', this.getService.bind(this));
        mediator.on('ws:message:orders:services:delete', this.deleteService.bind(this));

        //this.backend.bindMethods("findOrders", "/api/v1/organizations/${id}/orders", "GET");
        this.backend.bindMethods("findOrders", "/api/v1.1/orders", "GET");
        this.backend.bindMethods("createOrder", "/api/v1.1/orders", "POST");

        this.backend.bindMethods("findServices", "/api/v1.1/services", "GET");
        this.backend.bindMethods("findOrderServices", "/api/v1.1/orders/${id}/services", "GET");
        this.backend.bindMethods("createService", "/api/v1.1/services", "POST");

        this.backend.bindMethods("getService", "/api/v1.1/services/${id}", "GET");
        this.backend.bindMethods("deleteService", "/api/v1.1/services/${id}", "DELETE");

        log.info('Module `Orders` inited');
    }

    findOrders(data, options, callback){
        var accessToken = _.get(options, ['session', 'token', 'access_token']);

        var args = {
            path: {
                id: data.organization_id
            },
            token: accessToken
        };

        this.backend.call('findOrders', args, (err, data, statusCode) => {
            if (err) {
                return callback('INTERNAL_ERROR');
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, data);
            }
            return callback('INTERNAL_ERROR');
        });

    }

    onCreateOrder(data, options, callback){
        var accessToken = _.get(options, ['session', 'token', 'access_token']);

        var args = {
            data: {
                type: data.type,
                consultant: data.consultant || undefined,
                creator: data.creator || undefined,
                organization: data.organization || undefined
            },
            token: accessToken
        };

        this.backend.call('createOrder', args, (err, data, statusCode) => {
            if (err) {
                return callback('INTERNAL_ERROR');
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, data);
            }
            return callback('INTERNAL_ERROR');
        });
    }

    findServices(data, options, callback){
        var accessToken = _.get(options, ['session', 'token', 'access_token']);

        var args = {
            parameters: {
                type: data.service,
                start: data.start,
                stop: data.stop,
                size: 'long'
            },
            token: accessToken
        };
        //log.debug('findServices request', args);

        this.backend.call('findServices', args, (err, data, statusCode) => {
            //log.debug('findServices response', err, data, statusCode);
            if (err) {
                return callback('INTERNAL_ERROR');
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, data);
            }
            return callback('INTERNAL_ERROR');
        });
    }

    findOrderServices(data, options, callback){
        var accessToken = _.get(options, ['session', 'token', 'access_token']);

        var args = {
            path: {
                id: data.order_id
            },
            parameters: {
                size: 'long'
            },
            token: accessToken
        };

        this.backend.call('findOrderServices', args, (err, data, statusCode) => {
            if (err) {
                return callback('INTERNAL_ERROR');
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, data);
            }
            return callback('INTERNAL_ERROR');
        });
    }

    createService(data, options, callback){
        var accessToken = _.get(options, ['session', 'token', 'access_token']);

        var args = {
            data: {
                type: data.service,
                order_id: data.order_id,
                owner: parseInt(data.owner || options.session.user_id),
                title: data.title || 'Услуга ' + data.service,
                start: data.start,
                stop: data.stop,
                consultant: data.consultant || undefined,
                operator_id: data.operator_id || undefined,
                organization: data.organization || undefined,
                payment_type: data.payment_type || undefined,
                extend_data: _.assign({}, data.extend_data || {})
            },
            token: accessToken
        };

        this.backend.call('createService', args, (err, data, statusCode) => {
            if (err) {
                if (statusCode == 402) {
                    return callback('INSUFFICIENTLY_FUNDS');
                }
                return callback('INTERNAL_ERROR');
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, data);
            }
            return callback('INTERNAL_ERROR');
        });
    }

    getService(){}

    deleteService(data, options, callback){
        var accessToken = _.get(options, ['session', 'token', 'access_token']);
        var args = {
            path: {
                id: data.id
            },
            token: accessToken
        };

        this.backend.call('deleteService', args, (err, data, statusCode) => {
            if (err) {
                return callback('INTERNAL_ERROR');
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, data);
            }
            return callback('INTERNAL_ERROR');
        });
    }
}