var log = require('lib/logger')('MODULES');
import _ from 'lodash';

export default class Modules {
	constructor({config, connections}) {
		this.config = config;
		this.connections = connections;

		this.modules = {};

		this.init();
	}

	init() {
		for (let name in this.config) {
			if (this.config.hasOwnProperty(name)) {

				this.modules[name] = new (require('./' + name))({
					config: this.config[name],
					connections: _.pick(this.connections, this.config[name].transports)
				});
			}
		}
	}
}