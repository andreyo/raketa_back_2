import mediator from 'src/mediator';
import async from 'async';
import uuid from 'uuid';
import _ from 'lodash';
var log = require('lib/logger')('MODULE_SESSIONS');

import request from 'request';

class Sessions {
	constructor({config, connections}) {
		this.config = config;
		this.db = connections.db.getInstance();
		this.backend = connections.backend;

		mediator.on('ws:message:session:createByPassword', this.onAuthByPassword.bind(this));
		mediator.on('ws:message:session:getBySessionHash', this.onGetBySessionHash.bind(this));

		mediator.on('module:sessions:getSessionByHash', this.onGetBySessionHash.bind(this));

		this.backend.bindMethods("createToken", "/api/v1.1/oauth/v2/token", "POST");
		this.backend.bindMethods("getUser", "/api/v1.1/users/${id}", "GET");

		log.info('Module `Sessions` inited');
	}

	onAuthByPassword(data, options, callback) {
		if (!data.email || !data.password) {
			return callback('INVALID_PARAMS');
		}

		async.auto({
			createToken: this.createToken.bind(this, data),
			getUser: ['createToken', (cb, {createToken}) => {
				this.getUser(null, createToken.access_token, cb)
			}],
			saveSession: ['getUser', (cb, {getUser:user, createToken:token}) => {
				var session = new Session({
					user_id: user.id,
					parent_id: user.parent_id,
					creator_id: user.creator_id,
					username: user.username,
					native_first_name: user.native_first_name,
					native_middle_name: user.native_middle_name,
					native_last_name: user.native_last_name,
					translit_first_name: user.translit_first_name,
					translit_middle_name: user.translit_middle_name,
					translit_last_name: user.translit_last_name,
					phone_number: user.phone_number,
					enabled: user.enabled,
					organization: user.organization,
					roles: user.roles,
					permissions: user.permissions,
					env: {
						ua: data.userAgent
					},
					token: token
				}, this.db);

				session.save(cb);
			}]
		}, function(err, result) {
			callback(err, _.omit(result.saveSession, 'token', '_id'))
		});
	}

	onGetBySessionHash(data, options, callback) {
		this.findSession(data.hash, (err, result) => {
			if (data.forceUpdate) {
				var session = new Session(result, this.db);
				//console.log(session);
				this.getUser(session.user_id, session.token.access_token, (err, result) => {
					session.set(result);
					session.save((err, result) => {
						callback(err, _.omit(result, '_id'));
					});
				});
			}
			else {
				callback(err, _.omit(result, '_id'));
			}
		});
	}

	createToken(data, callback) {
		var formData = {
			grant_type: 'password',
			client_id: this.config.client_id,
			client_secret: this.config.client_secret,
			username: data.email,
			password: data.password
		};

		var args = {
			data: formData
		};

		this.backend.call('createToken', args, function(err, data, statusCode) {
			if (err) {
				if (err == 'invalid_grant') {
					return callback('INVALID_PARAMS');
				}
				return callback('INTERNAL_ERROR');
			}

			if (statusCode == 400) {
				return callback('INVALID_PARAMS');
			}
			else if (statusCode == 404) {
				return callback('INCORRECT_PASSWORD');
			}
			else if (statusCode >= 200 && statusCode < 400) {
				return callback(null, data);
			}
			return callback('INTERNAL_ERROR');
		});
	}

	getUser(id, accessToken, callback) {
		if (!id) id = 'me';
		var args = {
			path: {id: id},
			token: accessToken
		};

		this.backend.call('getUser', args, function(err, data, statusCode) {
			if (err) {
				return callback('INTERNAL_ERROR');
			}

			if (statusCode >= 200 && statusCode < 400) {
				return callback(null, data);
			}
			return callback('USER_NOT_FOUND');
		});
	}

	findSession(sessionHash, callback) {
		this.db.collection('sessions', function(err, sessions) {
			if (err) {
				log.error(err);
				return callback('INTERNAL_ERROR');
			}

			sessions.find({id: sessionHash}).toArray(function(err, result) {
				if (err) {
					log.error('findSession error: ', err);
					return callback('INTERNAL_ERROR');
				}

				callback(null, result[0]);
			});
		}.bind(this));
	}
}

class Session {
	constructor(options, db) {
		this.id = options.id || uuid.v4();
		this._id = options._id;
		this.user_id = options.user_id;
		this.parent_id = options.parent_id;
		this.username = options.username;
		this.native_first_name = options.native_first_name;
		this.native_middle_name = options.native_middle_name;
		this.native_last_name = options.native_last_name;
		this.translit_first_name = options.translit_first_name;
		this.translit_middle_name = options.translit_middle_name;
		this.translit_last_name = options.translit_last_name;
		this.phone_number = options.phone_number;
		this.enabled = options.enabled;
		this.organization = options.organization;
		this.roles = options.roles;
		this.permissions = options.permissions;
		this.env = options.env;
		this.token = options.token;

		this.db = db;
	}

	set(key, value) {
		if (key === 'id') return false;

		if (typeof key == 'string') {
			this[key] = value;
			return true;
		}

		if (_.isObject(key)) {
			let data = key;

			for (let i in data) if (data.hasOwnProperty(i)) {
				if (i === 'id') continue;
				this[i] = data[i];
			}
			return true;
		}

		return false;
	}

	save(callback) {
		this.db.collection('sessions', function(err, sessions) {
			if (err) {
				log.error(err);
				return callback('INTERNAL_ERROR');
			}

			var saveData = {
				id: this.id,
				_id: this._id,
				user_id: this.user_id,
				parent_id: this.parent_id,
				username: this.username,
				native_first_name: this.native_first_name,
				native_middle_name: this.native_middle_name,
				native_last_name: this.native_last_name,
				translit_first_name: this.translit_first_name,
				translit_middle_name: this.translit_middle_name,
				translit_last_name: this.translit_last_name,
				phone_number: this.phone_number,
				enabled: this.enabled,
				organization: this.organization,
				roles: this.roles,
				permissions: this.permissions,
				token: this.token,
				createdAt: new Date(),
				finishedAt: null
			};

			if (!this._id) {
				sessions.insert(saveData, function(err, result) {
					if (err) {
						log.error(err);
						return callback('INTERNAL_ERROR');
					}

					callback(null, result.ops[0]);
				});
			}
			else {
				sessions.update({id: this.id}, saveData, function(err, result) {
					if (err) {
						log.error(err);
						return callback('INTERNAL_ERROR');
					}
					callback(null, saveData);
				});
			}
		}.bind(this));
	}
}

export default Sessions;