import mediator from 'src/mediator';
//import {db, ObjectID} from './../../../lib/mongodb';
import async from 'async';
import uuid from 'uuid';
import _ from 'lodash';
var log = require('lib/logger')('MODULE_USERS');

import request from 'request';

class Users {
	constructor({config, connections}) {
		this.config = config;
		this.backend = connections.backend;

		mediator.on('ws:message:users:find', this.onGetUsers.bind(this));
		mediator.on('ws:message:users:create', this.onCreateUser.bind(this));
		mediator.on('ws:message:users:delete', this.onDeleteUser.bind(this));

		this.backend.bindMethods("findUsers", "/api/v1.1/organizations/${id}/users", "GET");
		this.backend.bindMethods("createUser", "/api/v1.1/organizations/${id}/users", "POST");
		this.backend.bindMethods("deleteUser", "/api/v1.1/organizations/${id}/users/${userId}", "DELETE");

		log.info('Module `Users` inited');
	}

	onGetUsers(data, options, callback) {
		var args = {
			path: {
				id: data.organization_id || options.session.organization.id
			},
			parameters: {
				limit: data.limit || 100,
				offset: data.offset || 0
			},
			token: options.session.token.access_token
		};

		this.backend.call('findUsers', args, (err, result, statusCode) => {
			if (err) {
				return callback(err);
			}
			if (statusCode >= 200 && statusCode < 400) {
				return callback(null, result);
			}
			callback(err);
		})
	}

	onCreateUser(data, options, callback) {
		var args = {
			path: {
				id: data.organization_id || options.session.organization.id
			},
			data: {
				username: data.email,
				email: data.email,
				plain_password: data.password,
				native_first_name: data.native_first_name,
				native_middle_name: data.native_middle_name,
				native_last_name: data.native_last_name,
				translit_first_name: data.translit_first_name,
				translit_middle_name: data.translit_middle_name,
				translit_last_name: data.translit_last_name,
				roles: data.roles
			},
			token: options.session.token.access_token
		};

		this.backend.call('createUser', args, (err, result, statusCode) => {
			if (err) {
				return callback(err);
			}
			if (statusCode >= 200 && statusCode < 400) {
				return callback(null, result);
			}
			callback(err);
		})
	}

	onDeleteUser(data, options, callback) {
		var args = {
			path: {
				id: data.organization_id || options.session.organization.id,
				userId: data.id
			},
			token: options.session.token.access_token
		};

		this.backend.call('deleteUser', args, (err, result, statusCode) => {
			if (err) {
				return callback(err);
			}
			if (statusCode >= 200 && statusCode < 400) {
				return callback(null, result);
			}
			callback(err);
		})
	}

	findUsers(accessToken, data, callback) {

	}

}

export default Users;