import mediator from 'src/mediator';
import async from 'async';
import uuid from 'uuid';
import _ from 'lodash';
var log = require('lib/logger')('MODULE_ORGANIZATIONS');

import request from 'request';

class Organizations {
    constructor({config, connections}) {
        this.config = config;
        this.backend = connections.backend;

        mediator.on('ws:message:organizations:find', this.onGetOrganizations.bind(this));
        mediator.on('ws:message:organizations:update', this.onUpdateOrganizations.bind(this));
        mediator.on('ws:message:organizations:create', this.onCreateOrganizations.bind(this));

        this.backend.bindMethods("findOrganizations", "/api/v1.1/organizations/${id}/organizations", "GET");
        this.backend.bindMethods("updateOrganization", "/api/v1.1/organizations/${id}", "PUT");
        this.backend.bindMethods("createOrganization", "/api/v1.1/organizations", "POST");

        log.info('Module `Organizations` inited');
    }

    onGetOrganizations(data, options, callback) {
        var args = {
            path: {
                id: data.organization_id || options.session.organization.id
            },
            token: options.session.token.access_token
        };

        this.backend.call('findOrganizations', args, (err, result, statusCode) => {
            if (err) {
                return callback(err);
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, result);
            }
            callback(err);
        })
    }

    onUpdateOrganizations(data, options, callback) {
        var args = {
            path: {
                id: data.organization_id
            },
            data: data.data,
            token: options.session.token.access_token
        };

        this.backend.call('updateOrganization', args, (err, result, statusCode) => {
            if (err) {
                return callback(err);
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, result);
            }
            callback(err);
        })
    }

    onCreateOrganizations(data, options, callback) {
        var args = {
            data: {
                name: data.name,
                email: data.email,
                parent: data.organization_id
            },
            token: options.session.token.access_token
        };

        this.backend.call('createOrganization', args, (err, result, statusCode) => {
            if (err) {
                return callback(err);
            }
            if (statusCode >= 200 && statusCode < 400) {
                return callback(null, result);
            }
            callback(err);
        })
    }

}

export default Organizations;