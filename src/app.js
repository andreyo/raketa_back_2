import SocketIO from 'socket.io';
import fs from 'fs';
import _ from 'lodash';
import Transports from 'lib/transports';
import ConfigLoader from 'lib/config-loader';
//import Services from './services';
import bodyParser from 'body-parser';
import Mediator from 'src/mediator';
import Modules from './modules';
import storage from './storage';

var log = require('lib/logger')('APP');

class Application {
	constructor(){
		this.logger = log;
		this.configs = new ConfigLoader({
			logger: this.logger
		}).config;

		this.initTransports(function(err, connections){

			storage.set('connections', connections);

			this.modules = new Modules({
				config: this.configs.modules,
				connections: connections
			});
		}.bind(this));
	}

	initTransports(cb) {
		this.transports = new Transports({
			config: this.configs.transports,
			logger: this.logger
		}, cb);
	}
}

process.on('uncaughtException', function(err) {
	log.error('uncaughtException:', err.stack)
});

module.exports = Application;