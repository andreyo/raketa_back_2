var path = require('path');
process.env.BABEL_CACHE_PATH = './.babel.json';
process.env.NODE_PATH = path.join(__dirname, '..');
require('module').Module._initPaths();
require('babel/register');
new (require('./app'))();