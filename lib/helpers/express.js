function checkRequires(requires, params) {
	for (let i = 0; i < requires.length; i++){
		if (!params.hasOwnProperty(requires[i])) {
			return false;
		}
	}
	return true;
}

function getMissedParams(requires, params){
	var missed = [];
	for (let i = 0; i < requires.length; i++){
		if (!params.hasOwnProperty(requires[i])) {
			missed.push(requires[i])
		}
	}
	return missed;
}

export {
	checkRequires,
	getMissedParams
}