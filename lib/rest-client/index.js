import Client from 'node-rest-client';

export default class RestClient {
	constructor({config}) {

		this.path = 'http://ag.newraketa.work:80';
		this.client = new Client();

		//this.bindMethods();
	}

	bindMethods() {
		//this.client.registerMethod('createToken', this.path + '/app_dev.php/oauth/v2/token', 'GET');
		//this.client.registerMethod('getSelfUser', this.path + '/app_dev.php/api/v1/users/me', 'GET');
		//this.client.registerMethod('getUser', this.path + '/app_dev.php/api/v1/users/${id}', 'GET');
		//this.client.registerMethod('getUsers', this.path + '/app_dev.php/api/v1/users/', 'GET');
	}

	getInstance() {
		return this.client;
	}
}