import async from 'async';
var log = require('lib/logger')('TRANSPORTS');

export default class Transports {
	constructor({config, logger}, cb) {
		this.config = config;
		this.transports = {
			rest_client: require('./rest_client'),
			mongodb: require('./mongodb'),
			socketio: require('./socketio')
		};
		this.connections = {};
		this.init(cb);
	}

	init(cb) {
		async.each(Object.keys(this.config), function(name, callback){
			let Transport = this.transports[this.config[name].transport];
			this.connections[name] = new Transport();
			this.connections[name].init(this.config[name], function() {
				log.info('Transport `'+name+'` ('+this.config[name].transport+') is ready');
				callback();
			}.bind(this));
		}.bind(this), function(err){
			log.info('All transports are ready');
			cb(err, this.connections);
		}.bind(this));
	}
}