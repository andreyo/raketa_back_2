var log = require('lib/logger')('TRANSPORT_REST');
import {Client} from 'node-rest-client';
import _ from 'lodash';

export default class RestClient {
	constructor() {
		this.config = {};
		this.client = new Client();
		this.endpoints = {};
	}

	init(options, callback) {
		this.config = options;

		let protocol = this.config.ssl && 'https' || 'http';
		this.path = [protocol, '://' + this.config.host + ':' + this.config.port].join('');

		callback();
	}

	bindMethods(method, endpoint, httpMethod) {
		if (this.endpoints.hasOwnProperty(method)) {
			throw new Error(`Method ${method} can't be use because it already used`);
		}
		this.endpoints[method] = endpoint;
		this.client.registerMethod(method, this.path + endpoint, httpMethod);
	}

	call(method, data, callback) {
		data.type = data.type || {};
        var parameters = data.parameters || {};
        for (let key in parameters) if (parameters.hasOwnProperty(key)) {
            if (parameters[key] === undefined)
                delete parameters[key];
        }

		var args = {
			path: _.assign({}, data.path || {}),
			parameters: parameters,
			headers: {
				'Content-Type': 'application/json'
			},
			data: _.assign({}, data.data || {}),
			requestConfig: _.assign({
				timeout: 30000,
				noDelay: true,
				keepAlive: true,
				keepAliveDelay: 1000
			}, data.requestConfig || {}),
			responseConfig: _.assign({}, data.responseConfig || {})
		};

		var logSign = data.token && data.token.substr(0, 6) || 'UNAUTH';

		log.debug('['+logSign+']', '-->', this.endpoints[method], _.pick(args, 'path', 'parameters', 'data'));

		if (data.token) {
			args.headers['Authorization'] = 'Bearer ' + data.token;
		}
		if (data.type == 'urlencoded') {
			args.headers['Content-Type'] = 'application/x-www-form-urlencoded';
		}

		var request = this.client.methods[method](args, (data, response) => {
			if (Buffer.isBuffer(data)) {
				data = data.toString() || '{}';
			}
			try {
				let parsed = _.isString(data) && data.length && JSON.parse(data) || data;
				log.debug('['+logSign+']', '<--', this.endpoints[method] + ': ' + response.statusCode, parsed);

				let error = null;
				if (response.statusCode >= 400) {
					error = parsed.error || parsed.errors || {};
				}

				if (error) {
					return callback(error, null, response.statusCode);
				}
				return callback(null, parsed, response.statusCode);
			} catch(e) {
				log.debug('['+logSign+']', '<--', this.endpoints[method] + ':' + response.statusCode, data);
				log.error('PARSE_ERROR 1', data);
				callback('PARSE_ERROR', data, response.statusCode);
			}
		});

		request.on('requestTimeout',function(req){
			log.error(`request to method \`${method}\` has expired`);
			callback('REQUEST_ERROR');
			req.abort();
		});

		request.on('responseTimeout',function(res){
			log.error(`request from method \`${method}\` has expired`);
			callback('REQUEST_ERROR', null, res.statusCode);
		});

		request.on('error',function(err){
			log.error(`Something went wrong on the client`, err);
			callback('REQUEST_ERROR');
		});
	}

	toUrlEncoded(data) {
		var str = [];
		for (let key in data) {
			str.push([key, encodeURIComponent(data[key])].join('='));
		}
		return str.join('&');
	}

	getInstance() {
		return this.client;
	}
}