import SocketIO from 'socket.io';
import mediator from 'src/mediator';
import _ from 'lodash';
import async from 'async';
var log = require('lib/logger')('TRANSPORT_WS');

export default class WS {
	constructor() {
		this.config = {};
		this.io = null;
		this.sockets = {};
	}

	init(options, callback) {
		this.config = options;

		this.io = new SocketIO(this.config.port, {
			transports: this.config.transports
		});

		this.io.on('connection', this._onConnect.bind(this));
		mediator.on('ws:emit', this._onEmit.bind(this));
		mediator.on('ws:broadcast', this._onBroadcast.bind(this));

		callback();
	}

	_onConnect(socket) {
		this.sockets[socket.id] = socket;
		mediator.emit('ws:connect', socket.id);

		socket.on('message', this._onMessage.bind(this, socket));
		socket.on('disconnect', this._onDisconnect.bind(this, socket));
	}

	_onDisconnect(socket) {
		delete this.sockets[socket.id];
		mediator.emit('ws:disconnect', socket.id);
	}

	_onMessage(socket, data) {
		data.socketId = socket.id;

		async.auto({
			session: (cb) => {
				mediator.emit('module:sessions:getSessionByHash', {hash: data.sessionHash, forceUpdate: data.forceUpdate}, {}, cb);
			}
		}, (err, {session}) => {
			//mediator.emit('ws:message', message.event, message.data, _.omit(message, 'event', 'data'));
			var requestContext = {
				requestId: data.requestId,
				sessionHash: data.sessionHash,
				session: session
			};
			mediator.emit('ws:message:' + data.event, data.data, requestContext, (err, result) => {
				if(!this.sockets[socket.id]) return;
				this.sockets[socket.id].emit('message', data.event+':response', {
					err: err,
					result: result
				}, data.requestId || null);
			});
		});
	}

	_onEmit(socketId, event, data, requestId) {
		if (this.sockets[socketId]) {
			this.sockets[socketId].emit('message', event, data, requestId);
		}
	}

	_onBroadcast(event, data) {
		this.io.sockets.emit('message', event, data);
	}

	getInstance() {
		return this.client;
	}
}