import {argv} from 'optimist';
import path from 'path';
import fs from 'fs';

class ConfigLoader {
	constructor(opts) {
		this.logger = opts.logger || console;
		this.env = argv.env;
		this.filePath = path.resolve(__dirname, './../../configs', argv.config+'.json');
		this.config = {};

		this.init();
	}

	init() {
		this.config = this.readFile();
	}

	readFile() {
		let data = fs.readFileSync(this.filePath),
			parsed = {};

		try {
			parsed = JSON.parse(data);
		} catch(e){
			this.logger.error('config parse error');
		}

		return parsed;
	}
}

module.exports = ConfigLoader;
