# Rocket Back #

This README would normally document whatever steps are necessary to get your application up and running.

### Для чего этот репозиторий? ###

* Node.js-прослойка между RocketFront и Platform

### Системные требования ###

* Unix-подобная система
* Node v4.x.x
* npm v3.x.x
* nginx
* MongoDB 3.0.x

### Установка ###

* Необходимо склонировать текущий репозиторий
* `npm i`
* Скопировать и применить конфиг nginx `/configs/nginx/development.conf`, подправив путь к проекту и хост (при необходимости внести в `/etc/hosts`)

### Запуск ###

* `cd ./bin`
* `./run.sh`

### Разработка ###
* `master` — главная ветка
* `release` — ветка для релизов
* все фичи в ветка с названием таска из JIRA (например, WEB-20), желательно с коротки описанием через дефис (WEB-20-change-version-of-api), чтобы понимать назначение ветки без необходимости смотреть в JIRA
* все коммиты с полным названием таска (WEB-20 Переход на апи 1.1), можно указывать в свободном стиле короткое описание что сделано в коммите
* pull request со своей веткой